# sxdc

Simple X Digital Clock.

## Dependencies

* libxcb
* xcb-util-wm
* xcb-util-image

## Installation

* $ make FG_COLOR=0x859900 BG_COLOR=0x002b36
* # make install

## Credits

The font is based on [Digital Font](https://opengameart.org/content/digital-font) by [Chasersgaming](https://opengameart.org/users/chasersgaming).

