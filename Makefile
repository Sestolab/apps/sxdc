FG_COLOR ?= 0x859900
BG_COLOR ?= 0x002b36

CFLAGS += -Wall -Wextra -pedantic -lxcb -lxcb-image -lxcb-icccm
CFLAGS += -DFG_COLOR=$(FG_COLOR) -DBG_COLOR=$(BG_COLOR)

PREFIX ?= /usr/local
CC ?= cc

all: sxdc

sxdc: sxdc.c
	$(CC) sxdc.c $(CFLAGS) -o sxdc

install: sxdc
	install -Dm755 sxdc -t $(DESTDIR)$(PREFIX)/bin

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/sxdc

clean:
	rm -f sxdc

.PHONY: all install uninstall clean

