#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <xcb/xcb.h>
#include <xcb/xcb_icccm.h>
#include <xcb/xcb_image.h>

#include "font.h"


#ifndef FG_COLOR
#define FG_COLOR 0x859900
#endif

#ifndef BG_COLOR
#define BG_COLOR 0x002b36
#endif

#define CHARACTER_SPACING 4
#define WINDOW_WIDTH (font_frame_width + CHARACTER_SPACING) * 5
#define WINDOW_HEIGHT font_height + CHARACTER_SPACING


void draw_frame(xcb_image_t *target, xcb_image_t *source, int frame, int tx)
{
	for (int y = 0; y < font_height; ++y)
	{
		for (int x = frame * font_frame_width; x < (frame + 1) * font_frame_width; ++x)
		{
			if (xcb_image_get_pixel(source, x, y) == 0) continue;
			xcb_image_put_pixel(target, (x - (frame * font_frame_width)) + tx, y + CHARACTER_SPACING / 2, FG_COLOR);
		}
	}
}

void draw_text(xcb_image_t *target, xcb_image_t *font, char *text)
{
	int x = CHARACTER_SPACING / 2;
	for (int i = 0; text[i] != '\0'; ++i)
	{
		switch (text[i])
		{
			case '0': case '1': case '2': case '3': case '4':
			case '5': case '6': case '7': case '8': case '9':
				draw_frame(target, font, text[i]-'0', x);
				break;
			case ':':
				draw_frame(target, font, 10, x);
				break;
			default: break;
		}
		x += font_frame_width + CHARACTER_SPACING;
	}
}


int main(void)
{

	xcb_connection_t *c = xcb_connect(NULL, NULL);

	if (xcb_connection_has_error(c))
	{
		fprintf(stderr, "Could not connect to the X server.\n");
		return 1;
	}

	xcb_screen_t *s = xcb_setup_roots_iterator(xcb_get_setup(c)).data;
	xcb_drawable_t w = xcb_generate_id(c);

	xcb_create_window(c, XCB_COPY_FROM_PARENT, w, s->root, 0, 0,
		WINDOW_WIDTH, WINDOW_HEIGHT, 0, XCB_WINDOW_CLASS_INPUT_OUTPUT, s->root_visual,
		XCB_CW_BACK_PIXEL|XCB_CW_EVENT_MASK, (uint32_t[]){BG_COLOR,
		XCB_EVENT_MASK_EXPOSURE|XCB_EVENT_MASK_KEY_PRESS|XCB_EVENT_MASK_RESIZE_REDIRECT, 0});

	xcb_gcontext_t gc = xcb_generate_id(c);
	xcb_create_gc(c, gc, w, 0, NULL);

	xcb_image_t *canvas = xcb_image_create_native(c, WINDOW_WIDTH, WINDOW_HEIGHT, XCB_IMAGE_FORMAT_Z_PIXMAP,
		s->root_depth, NULL, 0, NULL);
	xcb_image_t *font = xcb_image_create_native(c, font_width, font_height, XCB_IMAGE_FORMAT_Z_PIXMAP,
		s->root_depth, NULL, 0, font_data);

	xcb_change_property(c, XCB_PROP_MODE_REPLACE, w, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8, 4, "sxdc");
	xcb_change_property(c, XCB_PROP_MODE_REPLACE, w, XCB_ATOM_WM_CLASS, XCB_ATOM_STRING, 8, 4, "sxdc");

	xcb_size_hints_t hints;
	xcb_icccm_size_hints_set_min_size(&hints, WINDOW_WIDTH, WINDOW_HEIGHT);
	xcb_icccm_size_hints_set_max_size(&hints, WINDOW_WIDTH, WINDOW_HEIGHT);
	xcb_icccm_set_wm_size_hints(c, w, XCB_ATOM_WM_NORMAL_HINTS, &hints);
	xcb_atom_t delete_window_a = xcb_intern_atom_reply(c, xcb_intern_atom(c, 0, 16, "WM_DELETE_WINDOW"), NULL)->atom;
	xcb_atom_t wm_protoctols_a = xcb_intern_atom_reply(c, xcb_intern_atom(c, 0, 12, "WM_PROTOCOLS"), NULL)->atom;
	xcb_icccm_set_wm_protocols(c, w, wm_protoctols_a, 1, &delete_window_a);

	xcb_map_window(c, w);
	xcb_flush(c);

	char text[] = "00:00\0";
	short int tick = 1;
	short int should_exit = 0;
	time_t last_time;

	while (!should_exit)
	{
		xcb_generic_event_t *e = xcb_poll_for_event(c);
		time_t now = time(NULL);

		if (e != NULL) switch(e->response_type & ~0x80)
		{
			case XCB_EXPOSE:
				xcb_image_put(c, w, gc, canvas, 0, 0, 0);
				xcb_flush(c);
				break;
			case XCB_CLIENT_MESSAGE:
				if (((xcb_client_message_event_t *)e)->data.data32[0] == delete_window_a)
					should_exit = 1;
				break;
			case XCB_RESIZE_REQUEST:
				break;
			case XCB_KEY_PRESS:
				if (((xcb_key_press_event_t *)e)->detail == 24)
					should_exit = 1;
				break;
			default: break;
		}
		free(e);

		if (!last_time || difftime(now, last_time) >= 1)
		{
			last_time = now;

			for (int y = 0; y < canvas->height; ++y)
				for (int x = 0; x < canvas->width; ++x)
					xcb_image_put_pixel(canvas, x, y, BG_COLOR);

			struct tm *t = localtime(&now);
			strftime(text, sizeof(text), "%H:%M", t);
			if ((tick = tick % 2)) text[2] = ' ';
			draw_text(canvas, font, text);
			xcb_image_put(c, w, gc, canvas, 0, 0, 0);
			xcb_flush(c);
			++tick;
		}
		usleep(50000);
	}

	xcb_image_destroy(font);
	xcb_image_destroy(canvas);
	xcb_free_gc(c, gc);
	xcb_destroy_window(c, w);
	xcb_disconnect(c);
	return 0;
}

